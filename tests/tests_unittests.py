import os
import sys
import unittest
import xmlrpc.client

try:
    loopia_username = os.environ['loopia_username']
    loopia_password = os.environ['loopia_password']
    loopia_domain = os.environ['loopia_domain']
    except_cmd_args = {'credentials': {'loopia_username': loopia_username,
                                       'loopia_password': loopia_password},
                       'domains': {loopia_domain: {'sub_domains': ['test'],
                                                   'ttl': 3600}}}

    # Miss formated IP address since out of range, i.e. larger than 255
    except_ip = "123.321.123.321"

except KeyError as e:
    print(f'ERROR: {e} for LoopiaAPI not provided in user environment.')
    print('\t   Expecting loopia_username, loopia_password and loopia_domain to be set! Exiting...')
    exit(1)


class TestStringMethods(unittest.TestCase):

    @classmethod
    def buildUpClass(cls) -> None:
        # Runs once before all test
        pass

    @classmethod
    def tearDownClass(cls) -> None:
        # Runs once after all test
        pass

    def test_100_get_external_ip(self):
        from loopia_update_ip.lib.get_external_ip import get_external_ip

        response = get_external_ip()

        self.assertIsNotNone(response, False)

    def test_150_loopia_update_ip_import(self):
        try:
            import loopia_update_ip
            result = True
        except Exception as error_msg:
            print(f' ERROR: This happened: {error_msg}')
            result = False

        self.assertTrue(result, msg="Failed to import loopia_update_ip module")

    def test_151_loopia_update_ip_get_command_line_arguments(self):
        cmd_args = {}
        ip = ""

        try:
            sys.argv = ["loopia_update_ip",
                        "--username", loopia_username,
                        "--password", loopia_password,
                        "--ip", "123.321.123.321",
                        "--domain", "test." + loopia_domain]

            import loopia_update_ip.loopia_update_ip as loop_ip
            cmd_args, ip = loop_ip.get_command_line_arguments()

        except Exception as error_msg:
            print(f' ERROR: This happened: {error_msg}')

        self.assertDictEqual(cmd_args, except_cmd_args, msg="Failed to get_command_line_arguments")
        self.assertEqual(ip, except_ip, msg="Ip not as expected")

    def test_152_loopia_update_ip_get_config(self):

        config_expected = {'credentials': {'loopia_username': loopia_username,
                                           'loopia_password': loopia_password},
                           'domains': {loopia_domain: {'sub_domains': ['test'],
                                                       'ttl': 3600}}}
        config = {}

        try:
            sys.argv = ["loopia_update_ip",
                        "--username", loopia_username,
                        "--password", loopia_password,
                        "--ip", "123.321.123.321",
                        "--domain", "test." + loopia_domain]

            import loopia_update_ip.loopia_update_ip as loop_ip
            cmd_arguments, ip = loop_ip.get_command_line_arguments()
            config = loop_ip.get_config(cmd_arguments)

        except Exception as error_msg:
            print(f' ERROR: This happened: {error_msg}')

        self.assertDictEqual(config, config_expected, msg="The parsed config does not match expected")

    def test_153_loopia_update_ip_update(self):
        result = False

        try:

            sys.argv = ["loopia_update_ip",
                        "--username", loopia_username,
                        "--password", loopia_password,
                        "--ip", "123.321.123.321",
                        "--domain", "test." + loopia_domain]

            import loopia_update_ip.loopia_update_ip as loop_ip
            from unittest import mock

            loop_ip.get_command_line_arguments = mock.Mock(return_value=(except_cmd_args, except_ip))

            loop_ip.update()
            result = True
        except Exception as error_msg:
            print(f' ERROR: This happened: {error_msg}')

        self.assertEqual(result, True, msg="The parsed config does not match expected")

    def test_200_LoopiaAPI_import(self):
        try:
            from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
            LoopiaAPI(loopia_username, loopia_password, loopia_domain)
            result = True
        except Exception as error_msg:
            print(f' ERROR: This happened: {error_msg}')
            result = False

        self.assertTrue(result)

    def test_202_LoopiaAPI_get_subdomains(self):
        from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
        loop_object = LoopiaAPI(loopia_username, loopia_password, loopia_domain)

        sub_domains = loop_object.get_subdomains()

        if isinstance(sub_domains, list) and 'ERROR' not in sub_domains:
            status = True
        else:
            status = False

        self.assertTrue(status,
                        msg='Failed to get subdomains')

    def test_203_LoopiaAPI_add_subdomain(self):
        from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
        loop_object = LoopiaAPI(loopia_username, loopia_password, loopia_domain)

        sub_domains = loop_object.get_subdomains()

        if 'test' in sub_domains:
            print(' Subdomain "test" is already defined...')

        response = loop_object.add_subdomain('test')

        self.assertEqual(response,
                         'OK',
                         msg=f"Could not add subdomain to domain {loopia_domain}")

    def test_204_LoopiaAPI_check_subdomain(self):
        from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
        loop_object = LoopiaAPI(loopia_username, loopia_password, loopia_domain)

        is_defined = loop_object.check_if_subdomain_is_defined('test')

        self.assertTrue(is_defined,
                        msg=f"Expected subdomain could not be found: test.{loopia_domain}")

    def test_210_LoopiaAPI_add_zone_record(self):
        from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
        loop_object = LoopiaAPI(loopia_username, loopia_password, loopia_domain)

        response = loop_object.add_zone_record_subdomain('123.123.123.123',
                                               'test')

        self.assertEqual(response,
                         'OK',
                         msg=f'Could not add zone record for "test.{loopia_domain}"')

    def test_211_LoopiaAPI_add_zone_record_again(self):
        from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
        loop_object = LoopiaAPI(loopia_username, loopia_password, loopia_domain)

        subdomain = 'test'
        record_type = "A"

        response = loop_object.add_zone_record_subdomain('123.123.123.124',
                                                         subdomain,
                                                         record_type=record_type)

        self.assertEqual(response,
                         f'ERROR: Subdomain {subdomain}.{loopia_domain} with record of type {record_type} exists.',
                         msg=f'Could add zone record for "test.{loopia_domain}" which was not expected!')

    def test_212_LoopiaAPI_update_zone_record(self):
        from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
        loop_object = LoopiaAPI(loopia_username, loopia_password, loopia_domain)

        response = loop_object.update_zone_record_subdomain('123.123.123.124',
                                                  'test')

        self.assertEquals(response,
                          'OK',
                          msg=f'Could not add zone record for "test.{loopia_domain}"')

    def test_213_LoopiaAPI_update_subdomain_ip(self):
        from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
        loop_object = LoopiaAPI(loopia_username, loopia_password, loopia_domain)

        response = loop_object.update_subdomain_ip_address('123.123.123.124',
                                                           'test')

        self.assertEquals(response,
                          'OK',
                          msg=f'Could not add zone record for "test.{loopia_domain}"')

    def test_214_LoopiaAPI_remove_zone_records(self):
        from time import sleep
        from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
        loop_object = LoopiaAPI(loopia_username, loopia_password, loopia_domain)

        test_sub_domain = 'tested'

        # Create test subdomain and zone record
        response = loop_object.add_subdomain(test_sub_domain)
        print(f' Create subdomain response: {response}')
        response = loop_object.add_zone_record_subdomain('127.0.0.1', test_sub_domain)
        print(f' Create zone record response: {response}')

        # Wait a while and remove zone record and subdomain
        sleep(3)
        responses = loop_object.remove_zone_records_subdomain(test_sub_domain)

        response = loop_object.remove_subdomain(test_sub_domain)
        print(f' Remove subdomain response: {response}')

        # Check that all responses were OK
        result = all(response == 'OK' for response in responses)

        print(f' Was all zone records removed: {result}')

        self.assertTrue(result,
                        msg=f'Could not remove zone record for "{test_sub_domain}.{loopia_domain}"'
                            f' \n Responses: {responses}')

    def test_220_LoopiaAPI_remove_subdomain(self):
        from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
        loop_object = LoopiaAPI(loopia_username, loopia_password, loopia_domain)

        try:
            response = loop_object.remove_subdomain('test')
        except xmlrpc.client.Fault as error_msg:
            print(f'ERROR: Endpoint error message: {error_msg}')
            response = "Not OK"

        self.assertEquals(response,
                          'OK',
                          msg=f'Could not remove subdomain "test.{loopia_domain}"')

    def test_221_LoopiaAPI_addZoneRecord_multiple_types(self):
        from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
        loop_object = LoopiaAPI(loopia_username, loopia_password, loopia_domain)

        subdomain = 'test2'
        data_1 = f'message@{loopia_domain}'
        record_type_1 = "TXT"
        response_1 = ''

        data_2 = "222.222.111.111"
        record_type_2 = "A"
        response_2 = ''

        data_3 = f'message-check-1@{loopia_domain}'
        record_type_3 = "TXT"
        response_3 = ''

        try:
            # Remove subdomain if exists
            response = ''
            if subdomain in loop_object.get_subdomains():
                print(" Cleanup required")
                response = loop_object.remove_subdomain(subdomain)
            else:
                print(" No cleanup required")
                response = "OK"
            self.assertEqual(response, "OK", msg=f"ERROR: Subdomain could not be removed")
        except Exception as error_msg:
            print(f'ERROR: Cleanup before test failed with message {error_msg}!')

        try:
            response_1 = loop_object.add_zone_record_subdomain(data_1, subdomain, record_type=record_type_1)
        except xmlrpc.client.Fault as error_msg:
            print(f'ERROR: Endpoint error message: {error_msg}')
            response = "Not OK"

        try:
            response_2 = loop_object.add_zone_record_subdomain(data_2, subdomain, record_type=record_type_2)
        except xmlrpc.client.Fault as error_msg:
            print(f'ERROR: Endpoint error message: {error_msg}')
            response = "Not OK"

        try:
            response_3 = loop_object.add_zone_record_subdomain(data_3, subdomain, record_type=record_type_3)
        except xmlrpc.client.Fault as error_msg:
            print(f'ERROR: Endpoint error message: {error_msg}')
            response = "Not OK"

        self.assertEquals(response_1,
                          'OK',
                          msg='Could not add zone record type:'
                              f' {record_type_1} - data: {data_1} at {subdomain}.{loopia_domain}')
        self.assertEquals(response_2,
                          'OK',
                          msg='Could not add zone record type:'
                              f' {record_type_2} - data: {data_1} at {subdomain}.{loopia_domain}')
        self.assertEquals(response_3,
                          'OK',
                          msg='Could not add zone record type:'
                              f' {record_type_3} - data: {data_2} at {subdomain}.{loopia_domain}')

    def test_222_LoopiaAPI_updateZoneRecord_multiple_types(self):
        from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
        loop_object = LoopiaAPI(loopia_username, loopia_password, loopia_domain)

        subdomain = 'test2'
        data_1 = f'message@{loopia_domain}'
        data_1_2 = f'message-check-2@{loopia_domain}'
        record_type_1 = "TXT"
        response_1 = ''

        data_2 = "111.111.222.222"
        record_type_2 = "A"
        response_2 = ''

        data_1_3 = f'message-check-3@{loopia_domain}'
        response_3 = ''

        response_4 = ''

        try:
            response_1 = loop_object.replace_txt_zone_record_subdomain(data_1_2, data_1, subdomain)
        except xmlrpc.client.Fault as error_msg:
            print(f'ERROR: 1 - Endpoint error message: {error_msg}')
            response = "Not OK"

        try:
            response_2 = loop_object.update_zone_record_subdomain(data_2, subdomain, record_type=record_type_2)
        except xmlrpc.client.Fault as error_msg:
            print(f'ERROR: 2 - Endpoint error message: {error_msg}')
            response = "Not OK"

        try:
            response_3 = loop_object.replace_txt_zone_record_subdomain(data_1_3, data_1_2, subdomain)
        except xmlrpc.client.Fault as error_msg:
            print(f'ERROR: 3 - Endpoint error message: {error_msg}')
            response = "Not OK"

        try:
            response_4 = loop_object.remove_txt_zone_record_subdomain(data_1_3, subdomain)
        except xmlrpc.client.Fault as error_msg:
            print(f'ERROR: 4 - Endpoint error message: {error_msg}')
            response = "Not OK"

        self.assertEquals(response_1,
                          'OK',
                          msg='Could add zone record type:'
                              f' {record_type_1} - data: {data_1} at {subdomain}.{loopia_domain}')
        self.assertEquals(response_2,
                          'OK',
                          msg='Could add zone record type:'
                              f' {record_type_2} - data: {data_1} at {subdomain}.{loopia_domain}')
        self.assertEquals(response_3,
                          'OK',
                          msg='Could replace zone record:'
                              f' {record_type_2} - old data: {data_1} to new data {data_1_2} at'
                              f' {subdomain}.{loopia_domain}')

        self.assertEquals(response_4,
                          'OK',
                          msg='Could not remove TXT zone record :'
                              f' {record_type_2} - data: {data_2} at {subdomain}.{loopia_domain}')

    def test_223_LoopiaAPI_get_zone_record_subdomain_txt_records(self):
        from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
        loop_object = LoopiaAPI(loopia_username, loopia_password, loopia_domain)

        subdomain = 'test2'
        response = ''

        try:
            txt_records = loop_object.get_zone_record_subdomain_txt_records(subdomain)
            response = len(txt_records)
        except xmlrpc.client.Fault as error_msg:
            print(f'ERROR: TXT records were not received as expected: {error_msg}')

        self.assertEquals(response,
                          1,
                          msg='Could get TXT zone record at {subdomain}.{loopia_domain}')

    def test_224_LoopiaAPI_update_ip_adress(self):
        from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
        loop_object = LoopiaAPI(loopia_username, loopia_password, loopia_domain)

        subdomain = 'test2'
        ip_address_1 = "111.111.111.111"
        ip_address_2 = "222.222.222.222"
        response_1 = ''
        response_2 = ''

        try:
            response_1 = loop_object.update_subdomain_ip_address(ip_address_1, subdomain)
        except xmlrpc.client.Fault as error_msg:
            print('ERROR: A-record could not be updated as expected:'
                  f'\n\t Error message: {error_msg}'
                  f'\n\t Response message: {response_1}')

        try:
            response_2 = loop_object.update_subdomain_ip_address(ip_address_2, subdomain)
        except xmlrpc.client.Fault as error_msg:
            print('ERROR: A-record could not be updated as expected:'
                  f'\n\t Error message: {error_msg}'
                  f'\n\t Response message: {response_2}')

        self.assertEquals(response_1,
                          "OK",
                          msg=f'1 - Could update A-record for zone record at {subdomain}.{loopia_domain}')
        self.assertEquals(response_2,
                          "OK",
                          msg=f'2 - Could update A-record for zone record at {subdomain}.{loopia_domain}')

    def test_225_LoopiaAPI_remove_subdomain_test(self):
        from loopia_update_ip.lib.LoopiaAPI import LoopiaAPI
        loop_object = LoopiaAPI(loopia_username, loopia_password, loopia_domain)

        subdomain = 'test2'
        response = ''

        try:
            # Remove subdomain if exists
            if subdomain in loop_object.get_subdomains():
                print(" Cleanup required")
                response = loop_object.remove_subdomain(subdomain)
            else:
                print(" No cleanup required")
                response = "OK"
        except Exception as error_msg:
            print(f'ERROR: Cleanup before test failed with message {error_msg}!')
        self.assertEqual(response, "OK", msg=f"ERROR: Subdomain could not be removed")
