image: python:3.11-slim

stages:
  - check
  - build
  - test
  - publish


# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  PACKAGE_NAME: "$PACKAGE_NAME_AF"
  VARIABLES_FILE: ./variables.env


# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  paths:
    - .cache/pip

.before_python_stage:
  before_script:
    - python3 -m pip install --upgrade pip
    - python3 -m pip install setuptools wheel twine flake8 yamllint coverage

after_script:
  - echo  "Pipeline done..."
  - sleep 2
  - echo "Exiting"

# Print out system information
info:
  extends: .before_python_stage
  stage: check
  artifacts:
    reports:
      dotenv: version.env
  script:
    - pwd
    - whoami
    - which python
    - python -V
    - which pip
    - pip -V
    - echo "VERSION=$(python3 ./setup.py --version)" >> version.env
    - source version.env
    - echo "Currently $CI_PROJECT_NAME is at version $VERSION of the Python package"


# Linting
lint:
  extends: .before_python_stage
  stage: check
  script:
    - echo 'Linting code'
    # Check code quality
    - flake8 ./loopia_update_ip

# Build package
build:
  extends: .before_python_stage
  stage: build
  artifacts:
    paths:
      - dist/
    reports:
      dotenv: version.env
  script:
    - echo "Building $CI_PROJECT_NAME for version $VERSION of the Python package"
    - python3 setup.py sdist bdist_wheel

# Push to production repo only on master branch
build-docker:
#  image: $CI_REGISTRY/group/project/docker:20.10.16
  image: docker:20.10.16
  services:
    #- name: $CI_REGISTRY/group/project/docker:20.10.16-dind
    - name: docker:20.10.16-dind
      alias: docker
  stage: build
  script:
    - echo "Building docker image for $CI_PROJECT_NAME version $VERSION"
    - echo "$CI_JOB_TOKEN" | docker login --username $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - docker build -t registry.gitlab.com/niklasme/loopia-update-ip:latest -t registry.gitlab.com/niklasme/loopia-update-ip:$VERSION .
    - docker logout
test:
  extends: .before_python_stage
  stage: test
  script:
    - ls ./tests -la
    # Install from local newly built wheel
    - python3 -m pip install -r requirements.txt
    - python3 -m pip install "dist/$(ls dist/ | grep .whl)"
    # Run unit test found in folder "test/" (files matching "test*.py")
    # - coverage run -m unittest discover -s tests/
    - coverage run -m unittest tests/tests_unittests.py

    # Present coverage report
    - coverage report -m
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'

# Push to production repo only on master branch
publish-pypi:
  extends: .before_python_stage
  stage: publish
  only:
    - master
  script:
    - python3 setup.py sdist bdist_wheel
    - python3 -m twine upload --verbose -u ${USER_NAME} -p ${TOKEN} dist/*

# Push to production repo only on master branch
publish-docker:
  image: docker:20.10.16
  services:
    - name: docker:20.10.16-dind
      alias: docker
  stage: publish
  only:
    - master
  script:
    - echo "Publishing docker image for $CI_PROJECT_NAME version $VERSION"
    - echo "$CI_JOB_TOKEN" | docker login --username $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - docker build -t registry.gitlab.com/niklasme/loopia-update-ip:latest -t registry.gitlab.com/niklasme/loopia-update-ip:$VERSION .
    - docker push registry.gitlab.com/niklasme/loopia-update-ip:latest
    - docker push registry.gitlab.com/niklasme/loopia-update-ip:$VERSION
    - docker logout
