FROM python:3.11-slim-buster

WORKDIR /app

# Copy all required files
COPY . .

# Upgrade pip to latest version
RUN python3 -m pip install --upgrade pip
# Install all required dependencies
RUN python3 -m pip install -r requirements.txt
# Install application
RUN python3 -m pip install .

# Create paths
RUN mkdir /config
RUN mkdir /logs

# Add entry point
CMD ["loopia-update-ip-service"]